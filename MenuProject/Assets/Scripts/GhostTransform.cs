﻿using UnityEngine;
using System.Collections;

public class GhostTransform : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
            other.gameObject.GetComponent<PlayerControler>().GhostTransform();

    }
}
