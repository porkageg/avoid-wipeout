﻿using UnityEngine;
using System.Collections;

public class CheckPoint : MonoBehaviour {

    public GameObject respawnPoint;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
            other.gameObject.GetComponent<PlayerControler>().currentResPoint = respawnPoint.transform;

    }
}
