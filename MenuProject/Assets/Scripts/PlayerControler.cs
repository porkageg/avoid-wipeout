﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerControler : MonoBehaviour {
    [Header("Properties")]
    [Range(5, 50)]
    public float power = 20;
    [Range(5, 50)]
    public float maxSpeed = 30;
    [Range(1, 10)]
    public int life = 3;
    [Space(10)]

    public Transform currentResPoint;
    [SerializeField]
    private GameObject camera;
    [Space(10)]

    [Header("GUI")]
    [SerializeField]
    private Text displayLife;
    [SerializeField]
    private Text displaySpeed;

    [Header("Materials")]
    [SerializeField]
    private Material defaultMaterial;
    [SerializeField]
    private Material ghostMaterial;

    private Rigidbody rb;
    private Renderer rend;
    
    private enum States { DEFAULT, GHOST };
    private States currentState;
    private float timeTransform;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rend = GetComponent<Renderer>();
        displayLife.text = life.ToString();
        currentState = States.DEFAULT;
        timeTransform = 0.0f;
    }

    void Update()
    {
        // Transform timers
        if (currentState != States.DEFAULT)
        {
            timeTransform -= Time.deltaTime;
            if (timeTransform <= 0.0f)
            {
                //rend.material.color = new Color(rend.material.color.r, rend.material.color.g, rend.material.color.b, 90);
                DefaultTransform();
            }
           /* if (timeTransform <= 5.0f)
            {
                if ((int) timeTransform % 2 == 0)
                    rend.material.color = new Color(rend.material.color.r, rend.material.color.g, rend.material.color.b, 255);
                if ((int) timeTransform % 2 == 1)
                    rend.material.color = new Color(rend.material.color.R, rend.material.color.g, rend.material.color.b, 90);
            }*/
        }
    }

    void FixedUpdate()
    {
        //Get userInput
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        // Compute the direction of the ball
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        movement = camera.transform.TransformDirection(movement);
        movement.y = 0;

        // Apply force
        rb.AddForce(movement * power);

        // If speed of the ball > max speed, reduce the speed.
        if (rb.velocity.magnitude > maxSpeed)
        {
            rb.velocity = rb.velocity.normalized * maxSpeed;
        }

        // Display speed on screen
        displaySpeed.text = rb.velocity.magnitude.ToString("00") + " Km/H";
    }

    private void Reload()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Respawn()
    {
        life -= 1;

        if (life <= 0)
            this.Reload();
        displayLife.text = life.ToString();
        this.DefaultTransform();
        rb.Sleep();
        transform.position = currentResPoint.position;
        transform.rotation = currentResPoint.rotation;
    }

    public void DefaultTransform()
    {
        currentState = States.DEFAULT;
        gameObject.layer = LayerMask.NameToLayer("Default");
        timeTransform = 0.0f;
        rend.material = defaultMaterial;
    }

    public void GhostTransform()
    {
        currentState = States.GHOST;
        gameObject.layer = LayerMask.NameToLayer("Ghost");
        timeTransform = 15.0f;
        rend.material = ghostMaterial;
    }
}
