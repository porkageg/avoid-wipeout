﻿using UnityEngine;
using System.Collections;

public class Bumper : MonoBehaviour {

    [SerializeField]
    private int bumperForce = 150;

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player")
            other.gameObject.GetComponent<Rigidbody>().AddForce(Vector3.up * bumperForce);

    }
}
