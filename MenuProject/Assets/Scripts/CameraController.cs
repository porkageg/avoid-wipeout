﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    [SerializeField]
    private GameObject player;
    private Vector3 offset;
    private float boardTiltMax = 7f;
    private GameObject desiredPositionObject;

    private float rotationDamping = 5;
    private float movementDamping = 30;
    private float turnSpeed = 90;

    private float turnAngleH = 0.0f;
    // Use this for initialization
    void Start()
    {
        offset = transform.position - player.transform.position;
        desiredPositionObject = new GameObject("cameraFollow");
        desiredPositionObject.transform.position = transform.position;

        if (player == null)
        {
            Debug.LogError("Could not find object \"Player\" ! Aborting GameCamera load.");
            DestroyImmediate(gameObject);
        }
    }

    void Update()
    {
        if (Input.GetButtonDown("TurnH"))
        {
            if (Input.GetAxis("TurnH") > 0.0f)
                turnAngleH += turnSpeed;
            else if (Input.GetAxis("TurnH") < 0.0f)
                turnAngleH -= turnSpeed;
        }
    }

    void LateUpdate()
    {
        var heading = transform.position - player.transform.position;
        heading.y = 0f;
        var distance = heading.magnitude;
        var direction = heading / distance;

        Vector3 position = transform.position;
        var rotationVectorRight = Vector3.Cross(direction, Vector3.up);
        desiredPositionObject.transform.position = player.transform.position + offset;
        desiredPositionObject.transform.RotateAround(player.transform.position, Vector3.up, turnAngleH);
        desiredPositionObject.transform.RotateAround(player.transform.position, rotationVectorRight, -Input.GetAxisRaw("Vertical") * boardTiltMax);
        desiredPositionObject.transform.LookAt(player.transform.position);
        desiredPositionObject.transform.RotateAround(desiredPositionObject.transform.position, direction, -Input.GetAxisRaw("Horizontal") * boardTiltMax);
        transform.position = Vector3.Slerp(position, desiredPositionObject.transform.position, Time.deltaTime * movementDamping);
        transform.rotation = Quaternion.Lerp(transform.rotation, desiredPositionObject.transform.rotation, Time.deltaTime * rotationDamping);
        CenterCameraOnTarget();

    }

    private void CenterCameraOnTarget()
    {
        Plane plane = new Plane(transform.forward, player.transform.position);
        Ray ray = GetComponent<Camera>().ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0.0f));
        float distance;
        plane.Raycast(ray, out distance);

        var point = ray.GetPoint(distance);
        var offset = player.transform.position - point;
        transform.position += offset;
    }
}
