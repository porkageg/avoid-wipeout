﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	static public bool inPause = false;
	public GameObject pauseMenu;
	public GameObject HUD;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			inPause = !inPause;
		}
		if (inPause == true) {
			Pause ();
		} else {
			UnPause ();
		}
	}

	public void UnPause()
	{
		inPause = false;
		Time.timeScale = 1;
		HUD.SetActive(true);
		pauseMenu.SetActive (false);
	}

	public void Pause()
	{
		inPause = true;
		Time.timeScale = 0;
		HUD.SetActive(false);
		pauseMenu.SetActive (true);
	}

	public void goToMainMenu()
	{
		SceneManager.LoadScene(0);
	}


	public void quitGame()
	{
		Application.Quit ();
	}
}
