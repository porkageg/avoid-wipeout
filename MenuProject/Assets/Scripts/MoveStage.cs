﻿using UnityEngine;
using System.Collections;

public class MoveStage : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.rotation *= Quaternion.Euler(Input.GetAxis("Horizontal")*2, 0, Input.GetAxis("Vertical")*2);
        float nX = transform.rotation.eulerAngles.x, nY = transform.rotation.eulerAngles.y, nZ = transform.rotation.eulerAngles.z;

        transform.rotation = Quaternion.Euler(nX, nY, nZ);
    }
}
