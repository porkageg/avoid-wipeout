﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class EndOfLevel : MonoBehaviour {

    public int nextLevel;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
            SceneManager.LoadScene(nextLevel);

    }
}
