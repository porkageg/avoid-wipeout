﻿using UnityEngine;
using System.Collections;

public class DeadRespawn : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
            other.gameObject.GetComponent<PlayerControler>().Respawn();

    }
}
