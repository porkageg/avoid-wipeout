﻿using UnityEngine;
using System.Collections;

public class Booster : MonoBehaviour {

    [SerializeField]
    private float speed;

    void OnTriggerEnter(Collider other)
    {
        Rigidbody rb = other.GetComponent<Rigidbody>();
        //Vector3 other.GetComponent<Rigidbody>().velocity.normalized * speed;
        rb.velocity = rb.velocity.normalized * speed;
//        other.GetComponent<Rigidbody>().AddForce(force, ForceMode.VelocityChange);
    }
}
